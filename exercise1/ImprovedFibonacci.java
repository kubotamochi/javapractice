package exercise1;

class ImprovedFibonacci {
	static final int MAX_INDEX = 9;
	
	/**
	 * Output MAX_INDEX-count Fibonacci numbers.
	 * If the number is even, output "*" with it.
	 */
	public static void main(String args[]) {
		int lo = 0;
		int hi = 1;
		String mark;
		FibonacciElement[] elements = new FibonacciElement[MAX_INDEX];
		
		for(int i = 0; i < MAX_INDEX; i++) {
			elements[i] = new FibonacciElement(hi);
			hi = lo + hi;
			lo = hi - lo;
		}
		
		for(int i = 0; i < MAX_INDEX; i++) {
			FibonacciElement element = elements[i];
			if(element.isEven()) {
				mark = " *";
			} else {
				mark = "";
			}
			System.out.println(i + 1 + ": " + element.getNumber() + mark);
		}
	}
}