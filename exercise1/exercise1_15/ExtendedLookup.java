package exercise1.exercise1_15;

public interface ExtendedLookup extends Lookup {
	
	/**
	 * Makes class keep param object with param name. 
	 * @param name
	 * @param item
	 */
	void add(String name, Object item);
	
	/**
	 * Removes object related to param name from class. 
	 * @param name
	 * @return removed object
	 */
	Object remove(String name);

}
