package exercise1.exercise1_15;

public interface Lookup {
	/**
	 * Return object which relates to param name. 
	 * If there isn't such object, return null.
	 * @param name
	 * @return a related object
	 */
	Object find(String name);
}
