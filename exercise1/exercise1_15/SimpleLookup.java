package exercise1.exercise1_15;

import java.util.HashMap;
import java.util.Map;

/**
 * a class which has a hashmap of Object and implements ExtendedLookup interface
 * @author kubotamochi
 *
 */
public class SimpleLookup implements ExtendedLookup {
	
	private Map<String, Object> objects = new HashMap<String, Object>();

	@Override
	public Object find(String name) {
		if(objects.containsKey(name)) {
			return objects.get(name);
		}
		return null;
	}

	@Override
	public void add(String name, Object item) {
		objects.put(name, item);

	}

	@Override
	public Object remove(String name) {
		if(objects.containsKey(name)) {
			return objects.remove(name);
		}
		return null;
	}

}
