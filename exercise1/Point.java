class Point {
	/** the value of x-coordinate */
	public double x;
	/** the value of y-coordinate */
	public double y;
	
	public void clear() {
		x = 0.0;
		y = 0.0;
	}
	
	public double distance(Point that) {
		double xdiff = x - that.x;
		double ydiff = x - that.y;
		
		return Math.sqrt(xdiff * xdiff + ydiff * ydiff);
	}
	
	public void move(double x, double y) {
		this.x = x;
		this.y = y;
	}
	
	/** moves to the specified point. */
	public void move(Point goal) {
		this.x = goal.x;
		this.y = goal.y;
	}
}