package exercise1.exercise1_16;

import java.io.IOException;

/**
 * an Exception which is thrown when IOException occurred.
 * This Excepion has an IOException and a name of the set the IOException occurred in.
 * @author kubotamochi
 *
 */
public class BadDataSetException extends Exception {

	private String setName;
	private IOException ioException;
	
	public BadDataSetException(String setName, IOException ioException) {
		this.setName = setName;
		this.ioException = ioException;
	}

	public String getSetName() {
		return setName;
	}

	public void setSetName(String setName) {
		this.setName = setName;
	}

	public IOException getIoException() {
		return ioException;
	}

	public void setIoException(IOException ioException) {
		this.ioException = ioException;
	}
}
