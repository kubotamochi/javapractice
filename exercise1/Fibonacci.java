class Fibonacci {
	//Output Fibonacci Numbers less than 50
	static final int MAX = 50;
	static final String TITLE = "*** Fibonacci Numbers less than 50 ***";
	public static void main(String[] args) {
		int lo = 0;
		int hi = 1;
		System.out.println(TITLE);
		
		int[] elements = new int[10];
		int index = 0;
		while(hi < MAX) {
			//When there is no room in 'elements', enlarge it
			if(index >= elements.length) {
				int[] temp = elements;
				elements = new int[index + 10];
				for(int i = 0; i < temp.length; i++) {
					elements[i] = temp[i];
				}
			}
			
			elements[index] = hi;
			hi = hi + lo;
			lo = hi - lo;
			
			index++;
		}
		
		//Output when the target has a number
		for(int i = 0; i < index; i++) {
			System.out.println(elements[i]);
		}
	}
}