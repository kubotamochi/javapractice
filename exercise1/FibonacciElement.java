package exercise1;
/**
 * This class has a number and whether the number is even or not.
 */
public class FibonacciElement {
	private int number;
	private boolean even;
	
	public FibonacciElement(int number) {
		this.number = number;
		if(number % 2 == 0) {
			this.even = true;
		} else {
			this.even = false;
		}
	}
	
	public int getNumber() {
		return this.number;
	}
	
	public boolean isEven() {
		return this.even;
	}
}