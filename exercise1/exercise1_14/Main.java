package exercise1.exercise1_14;

import java.util.ArrayList;
import java.util.List;

public class Main {
	
	static String[] SCALE = {"ド", "レ", "ミ", "ファ", "ソ", "ラ", "シ"};
	static int SONG_CNT = 5;
	
	public static void main(String args[]) {
		Tape tape = new Tape();
		
		//create songs and make a tape
		for(int i = 0; i < SONG_CNT; i++) {
			List<String> melody = new ArrayList<String>();
			for(int j = 0; j < 10; j++) {
				melody.add(SCALE[(int)(Math.random() * SCALE.length)]);
			}
			tape.record(new Song("title" + (i + 1), melody));
		}
		
		WalkmanTalkable myWalk = new WalkmanTalkable();
		myWalk.playAll();
		myWalk.setTape(tape);
		myWalk.playAll();
		myWalk.setListener(new Listener());
		myWalk.setSecondListener(new Listener());
		myWalk.playAll();
		myWalk.play(10);
		
	}

}
