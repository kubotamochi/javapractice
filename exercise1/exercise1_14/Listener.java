package exercise1.exercise1_14;

import java.util.List;

/**
 *A class which recieves melody and output it. 
 * @author kubotamochi
 *
 */
public class Listener {
	/** a phrase which is used for speaking to other listener  */
	static final String CALL = "この曲どう？";
	/** a phrases which are used for response */
	static final String[] RESPONSES = {"大好き！!", "好き！", "まあ・・・", "微妙", "クソ", "え？なんち？"};
	
	private String name;
	
	/**
	 * recieves melody as param and output it.
	 * @param melodies
	 */
	public void listen(List<String> melodies) {
		for(String melody : melodies) {
			System.out.print(melody + " ");
		}
		System.out.println("\n");
	}
	
	/**
	 * speaks to other listener.
	 * when target reposnse a need-call phrase, execute one more time.
	 * @param target
	 */
	public void talk(Listener target) {
		final int NEED_RECALL = 5;
		System.out.println(CALL);
		String res = target.response();
		if(res == RESPONSES[NEED_RECALL]) {
			talk(target);
		}
	}
	
	/**
	 * response a phrase.
	 * @return
	 */
	public String response() {
		String res = RESPONSES[(int)(Math.random()*RESPONSES.length)];
		System.out.println(res);
		return res;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
}
