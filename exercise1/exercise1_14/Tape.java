package exercise1.exercise1_14;

import java.util.ArrayList;
import java.util.List;

/**
 * Has a list of Song, add Songs to the list, or get info of a song.
 * @author kubotamochi
 *
 */
public class Tape {
	/** A list of songs which are recorded in tape */
	private List<Song> trackList = new ArrayList<Song>();
	
	/**
	 * add a param song to the list
	 * @param song
	 */
	public void record(Song song) {
		trackList.add(song);
	}
	
	/**
	 * get song's name
	 * @param trackNo
	 * @return
	 */
	public String getTitle(int trackNo) {
		return trackList.get(trackNo).getTitle();
	}
	
	/**
	 * get song's melody
	 * @param trackNo
	 * @return
	 */
	public List<String> searchSong(int trackNo) {
		return trackList.get(trackNo).getMelody();
	}

	
	public List<Song> getTrackList() {
		return trackList;
	}

	public void setTrackList(List<Song> trackList) {
		this.trackList = trackList;
	}
	
	
}
