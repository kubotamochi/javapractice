package exercise1.exercise1_14;

import java.util.List;

public class WalkmanTalkable extends WalkmanDouble {
	public void play(int trackNo) {
		Listener first = getListener();
		Listener second = getSecondListener();
		Tape tape = getTape();
		
		if(first == null && second == null) {
			System.out.println("オーディオ機器が接続されていません");
			return;
		}
		if(tape == null) {
			System.out.println("テープがセットされていません");
			return;
		}
		try {
			System.out.println(tape.getTitle(trackNo));
			List<String> melody = tape.searchSong(trackNo);
			if(first != null) {
				first.listen(melody);
			}
			
			if(second != null) {
				second.listen(melody);
			}
			first.talk(second);
		} catch(IndexOutOfBoundsException e) {
			System.out.println("指定した番号の曲がありません。" + (tape.getTrackList().size()-1) + "以下の数値を指定してください");
		}
	
	}
}
