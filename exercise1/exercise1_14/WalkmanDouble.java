package exercise1.exercise1_14;

import java.util.List;

/**
 * Walkman which sends melody to two listener at most at the same time.
 * @author kubotamochi
 *
 */
public class WalkmanDouble extends Walkman {
	/** secondary listener */
	private Listener secondListener;
	
	@Override
	public void play(int trackNo) {
		Listener first = getListener();
		Listener second = getSecondListener();
		Tape tape = getTape();
		
		if(first == null && second == null) {
			System.out.println("オーディオ機器が接続されていません");
			return;
		}
		if(tape == null) {
			System.out.println("テープがセットされていません");
			return;
		}
		try {
			System.out.println(tape.getTitle(trackNo));
			List<String> melody = tape.searchSong(trackNo);
			if(first != null) {
				first.listen(melody);
			}
			
			if(second != null) {
				second.listen(melody);
			}
		} catch(IndexOutOfBoundsException e) {
			System.out.println("指定した番号の曲がありません。" + (tape.getTrackList().size()-1) + "以下の数値を指定してください");
		}
		
	}

	public Listener getSecondListener() {
		return secondListener;
	}

	public void setSecondListener(Listener secondListener) {
		this.secondListener = secondListener;
	}

}
