package exercise1.exercise1_14;

import java.util.List;

/**
 * gets melody from a Tape and sends it to a Listener
 * @author kubotamochi
 *
 */
public class Walkman {

	/** Lisener who receives melody */
	private Listener listener;
	
	/** Tape which has Songs */
	private Tape tape;
	
	/**
	 * Sends melody of all songs in tape.
	 */
	public void playAll() {
		if(tape == null) {
			System.out.println("テープがセットされていません");
			return;
		}
		if(listener == null) {
			System.out.println("オーディオ機器が接続されていません");
			return;
		}
		int trackCnt = tape.getTrackList().size();
		for(int i = 0; i < trackCnt; i++) {
			play(i);
		}
	}
	
	/**
	 * Sends melody of a specific song which is specified by param. 
	 * @param trackNo
	 */
	public void play(int trackNo) {
		if(listener == null) {
			System.out.println("オーディオ機器が接続されていません");
			return;
		}
		if(tape == null) {
			System.out.println("テープがセットされていません");
			return;
		}
		try {
			System.out.println(tape.getTitle(trackNo));
			List<String> melody = tape.searchSong(trackNo);
			listener.listen(melody);
		} catch(IndexOutOfBoundsException e) {
			System.out.println("指定した番号の曲がありません。" + (tape.getTrackList().size()-1) + "以下の数値を指定してください");
		}
		
	}
 
	public Listener getListener() {
		return listener;
	}

	public void setListener(Listener listener) {
		this.listener = listener;
	}

	public Tape getTape() {
		return tape;
	}

	public void setTape(Tape tape) {
		this.tape = tape;
	}
	
	
}
