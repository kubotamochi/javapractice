package exercise1.exercise1_14;

import java.util.ArrayList;
import java.util.List;

/**
 * A class which has a title and melody
 * @author kubotamochi
 *
 */
public class Song {
	/** Song's title */
	private String title;
	
	/** Song's melody which consists of Strings */
	private List<String> melody = new ArrayList<String>();

	
	public Song(String title, List<String> melody) {
		this.title = title;
		this.melody = melody;
	}
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<String> getMelody() {
		return melody;
	}

	public void setMelody(List<String> melody) {
		this.melody = melody;
	}

}
