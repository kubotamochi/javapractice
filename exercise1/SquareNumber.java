class SquareNumber {
	//Output Square Numbers until 100
	static final String TITLE = "Square Numbers";
	public static void main(String[] args) {
		System.out.println(TITLE);
		for(int i = 1; i <= 10; i++) {
			System.out.println(i * i);
		}
	}
}