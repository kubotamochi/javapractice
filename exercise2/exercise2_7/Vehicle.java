package exercise2.exercise2_7;

/**
 * A class which represents vehicle.
 * @author kubotamochi
 *
 */
public class Vehicle {
	/** the vehicle's IDNumber */
	private final int id;
	/** speed the vehicle goes at */
	private double speed;
	/** a direction　angle the vehicle goes in */
	private double direction;
	/** the vehicle owner's name */
	private String owner;
	/** the IDNumber which is given to next vehicle object */
	private static int nextID;
	
	/** create a Vehicle object with setting id */
	public Vehicle() {
		id = nextID++;
	}
	
	/** create a Vehicle object with setting id and ownersName */
	public Vehicle(String firstOwner) {
		this();
		owner = firstOwner;
	}
	
	public int getId() {
		return id;
	}
	public double getSpeed() {
		return speed;
	}
	public void setSpeed(double speed) {
		this.speed = speed;
	}
	public double getDirection() {
		return direction;
	}
	public void setDirection(double direction) {
		this.direction = direction;
	}
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	public static int getNextID() {
		return nextID;
	}
	
	public static void main(String[] args) {
		final String OWNER = "Kubota";
		Vehicle[] vehicles = new Vehicle[5];
		for(int i = 0; i < vehicles.length; i++) {
			vehicles[i] = new Vehicle(OWNER);
			vehicles[i].setSpeed(Math.random());
			vehicles[i].setDirection(Math.random() * 360);
		}
		
		for(Vehicle vehicle : vehicles) {
			System.out.println("ID番号 : " + vehicle.getId());
			System.out.println("速度 : " + vehicle.getSpeed());
			System.out.println("方向 : " +vehicle.getDirection());
			System.out.println("所有者 : " + vehicle.getOwner());
			System.out.println();
		}		
	}
}
