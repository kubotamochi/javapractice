package exercise2.exercise2_2;

/**
 * LinkedList class which has a value and reference to the next in the list.
 * @author kubotamochi
 *
 */
public class LinkedList {

	/** the value of the object */
	private Object value;
	/** reference to the next object in the list */
	private LinkedList next;
	
	public Object getValue() {
		return value;
	}
	public void setValue(Object value) {
		this.value = value;
	}
	public LinkedList getNext() {
		return next;
	}
	public void setNext(LinkedList next) {
		this.next = next;
	}
	
	
}
