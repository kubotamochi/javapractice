package exercise2.exercise2_3;

/**
 * A class which represents vehicle.
 * @author kubotamochi
 *
 */
public class Vehicle {
	/** the vehicle's IDNumber */
	private int id;
	/** speed the vehicle goes at */
	private double speed;
	/** a direction　angle the vehicle goes in */
	private double direction;
	/** the vehicle owner's name */
	private String owner;
	/** the IDNumber which is given to next vehicle object */
	private static int nextID;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public double getSpeed() {
		return speed;
	}
	public void setSpeed(double speed) {
		this.speed = speed;
	}
	public double getDirection() {
		return direction;
	}
	public void setDirection(double direction) {
		this.direction = direction;
	}
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	public static int getNextID() {
		return nextID;
	}
	
}
