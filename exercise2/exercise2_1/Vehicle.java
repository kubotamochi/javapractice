package exercise2.exercise2_1;

/**
 * A class which represents vehicle.
 * @author kubotamochi
 *
 */
public class Vehicle {

	/** speed the vehicle goes at */
	private double speed;
	/** a direction　angle the vehicle goes in. */
	private double direction;
	/** the vehicle owner's name */
	private String owner;
	
	public double getSpeed() {
		return speed;
	}
	public void setSpeed(double speed) {
		this.speed = speed;
	}
	public double getDirection() {
		return direction;
	}
	public void setDirection(double direction) {
		this.direction = direction;
	}
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	
	
}
