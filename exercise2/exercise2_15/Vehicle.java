package exercise2.exercise2_15;

/**
 * A class which represents vehicle.
 * @author kubotamochi
 *
 */
public class Vehicle {
	/** the vehicle's IDNumber */
	private final int id;
	/** speed the vehicle goes at */
	private double speed;
	/** a direction　angle the vehicle goes in */
	private double direction;
	/** the vehicle owner's name */
	private String owner;
	/** the IDNumber which is given to next vehicle object */
	private static int nextID;
	
	/** create a Vehicle object with setting id */
	public Vehicle() {
		id = nextID++;
	}
	
	/** create a Vehicle object with setting id and ownersName */
	public Vehicle(String firstOwner) {
		this();
		owner = firstOwner;
	}
	
	@Override
	public String toString() {
		final String SPLIT = ", ";
		StringBuilder str = new StringBuilder();
		str.append(id);
		str.append("(");
		if(owner == null) {
			str.append("NotBeOwned");
		} else {
			str.append(owner);
		}
		str.append(SPLIT);
		str.append(speed);
		str.append(SPLIT);
		str.append(direction);
		str.append(")");
		
		return str.toString();
	}
	
	/**
	 * Changes this speed to param value.
	 * @param speed
	 */
	public void changeSpeed(double speed) {
		this.speed = speed;
	}
	/**
	 * Sets this speed to 0.
	 */
	public void stop() {
		this.speed = 0;
	}
	public int getId() {
		return id;
	}
	public double getSpeed() {
		return speed;
	}
	public double getDirection() {
		return direction;
	}
	public void setDirection(double direction) {
		this.direction = direction;
	}
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	public static int getNextID() {
		return nextID;
	}
	
	/**
	 * return the max ID number in all numbers which Vehicle class has given.
	 * @return the Max ID number
	 */
	public static int getMaxID() {
		return nextID - 1;
	}
	
	public static void main(String[] args) {
		final String OWNER = "Kubota";
		Vehicle[] vehicles = new Vehicle[5];
		for(int i = 0; i < vehicles.length; i++) {
			vehicles[i] = new Vehicle(OWNER);
			vehicles[i].changeSpeed(Math.random());
			vehicles[i].setDirection(Math.random() * 360);
		}
		
		for(Vehicle vehicle : vehicles) {
			System.out.println("ID番号 : " + vehicle.getId());
			System.out.println("速度 : " + vehicle.getSpeed());
			System.out.println("方向 : " +vehicle.getDirection());
			System.out.println("所有者 : " + vehicle.getOwner());
			System.out.println();
		}		
	}
}
