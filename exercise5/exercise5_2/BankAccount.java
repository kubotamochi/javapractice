package exercise5.exercise5_2;

import java.util.ArrayList;
import java.util.List;

/**
 * The BankAccount that can transfer money and record history of account
 * @author kubotamochi
 *
 */
public class BankAccount {
	private long number;
	private long balance;
	private Action lastAct;
	private List<Action> actions = new ArrayList<Action>();
	public static final int MAX_ACTION_NUM = 10;
	private static long givenNumber = 0;
	
	public BankAccount(long firstDeposit) {
		number = givenNumber++;
		balance = firstDeposit;
	}
	
	public BankAccount() {
		this(0);
	}
	/**
	 * The inner class of BankAccount class.
	 * This class represents an account 
	 * @author kubotamochi
	 *
	 */
	public class Action {
		private String act;
		private long amount;
		Action(String act, long amount) {
			this.act = act;
			this.amount = amount;
		}
		public String toString() {
			return number + ":" + act + " " + amount;
		}
	}
	
	/**
	 * Deposits in this account
	 * @param amount
	 */
	public void deposit(long amount) {
		balance += amount;
		lastAct = new Action("deposit", amount);
		saveAction(lastAct);
	}
	
	/**
	 * withdraw from this account
	 * @param amount
	 */
	public void withdraw(long amount) {
		balance -= amount;
		lastAct = new Action("withdraw", amount);
		saveAction(lastAct);
	}
	
	private void saveAction(Action action) {
		if(actions.size() >= MAX_ACTION_NUM) {
			for(int i = 0; i < actions.size()-1; i++) {
				actions.set(i, actions.get(i+1));
			}
			actions.remove(MAX_ACTION_NUM - 1);
		}
		
		actions.add(action);
	}
	
	
	
	/**
	 * The inner class of BankAccount class
	 * This class 
	 * @author kubotamochi
	 *
	 */
	public static class History {
		private int index;
		private List<Action> actions;
		public History(List<Action> actions) {
			this.actions = actions;
			index = 0;
		}
		
		/**
		 * Gets history of action in order
		 * @return An Action object or null
		 */
		public Action next() {			
			if(actions.size() <= index) {
				return null;
			}
			return actions.get(index++);
		}
	}
	
	public History history() {
		return new History(actions);
	}
	
	public long getBalance() {
		return balance;
	}
	
	public long getNumber() {
		return number;
	}
	
	public static void main(String args[]) {
		BankAccount account = new BankAccount();
		for(int i = 1; i <= 4; i++) {
			account.deposit(i * 100);
			account.withdraw(i * 10);
		}
		History h = account.history();
		Action act;
		System.out.println("1回目");
		while((act = h.next()) != null) {
			System.out.println(act.toString());
		}
		account.deposit(999);
		account.deposit(999);
		account.deposit(999);
		account.deposit(999);
		h = account.history();
		System.out.println("2回目");
		while((act = h.next()) != null) {			
			System.out.println(act.toString());
		}
		BankAccount account2 = new BankAccount(200);
		account2.deposit(200);
		account2.withdraw(10);
		System.out.println("第2口座");
		h = account2.history();
		while((act = h.next()) != null) {			
			System.out.println(act.toString());
		}
	}

}
