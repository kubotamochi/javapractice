package exercise4.exercise4_3;




/**
 * LinkedList class which has a value and reference to the next in the list.
 * @author kubotamochi
 *
 */
interface LinkedList<T> {
	/**
	 * count nodes following this node in the list
	 * @return the number of nodes
	 */
	int countNode();
	
	/**
	 * get the value of this node
	 * @return the value of this node
	 */
	T getValue();
	
	/**
	 * get the LinkedList object which is next of this
	 * @return LinkedList object
	 */
	LinkedList<T> getNext();
	
	/**
	 * create a new node following this node
	 * @param value of the new node
	 */
	void setNext(T value);
	
}
