package exercise4.exercise4_3;

import exercise3.exercise3_10.vehicles.Vehicle;



/**
 * The class which implements LinkedList interface.
 * The class has a value and reference to the next in the list.
 * @author kubotamochi
 *
 */
public class LinkedListImpl<T> implements LinkedList<T>, Cloneable {

	/** the value of this object */
	private T value;
	/** reference to the next object in the list */
	private LinkedListImpl<T> next;
	
	/** The constructor with object's value parameter */
	public LinkedListImpl(T value) {
		this.value = value;
	}
	
	/** The constructor with object's value and the next object parameters */
	public LinkedListImpl(T value, LinkedListImpl<T> next) {
		this(value);
		this.next = next;
	}
	
	@Override
	public LinkedListImpl<T> clone() throws CloneNotSupportedException {
		@SuppressWarnings("unchecked")
		LinkedListImpl<T> cloned = (LinkedListImpl<T>)super.clone();
		if(this.next == null) {
			return cloned;
		}
		
		LinkedListImpl<T> originalNext = this;
		LinkedListImpl<T> target = cloned;
		
		while((originalNext = originalNext.next) != null) {
			target.next = new LinkedListImpl<T>(originalNext.value);
			target = target.next;
		}
		
		return cloned;
		
	}
	@Override
	public String toString() {
		StringBuilder str = new StringBuilder();
		str.append(value.toString());
		str.append(" ");
		if(next != null) {
			str.append("before of ");
			str.append(next.toString());
		}
		
		return str.toString();
	}

	/**
	 * Counts nodes of the List. 
	 * @return number of nodes.
	 */
	public int countNode() {
		int cnt = 1;
		LinkedListImpl<T> node = this;
		while((node = node.next) != null) {
			cnt++;
		}
		return cnt;
	}
	
	public T getValue() {
		return value;
	}
	public LinkedListImpl<T> getNext() {
		return next;
	}
	public void setNext(T value) {
		this.next = new LinkedListImpl<T>(value);
	}

	public static void main(String[] args) throws CloneNotSupportedException {
		final int VEHICLE_NUM = 5;
		final String OWNER = "Kubota";
		Vehicle[] vehicles = new Vehicle[VEHICLE_NUM];
		
		 
		for(int i = 0; i < vehicles.length; i++) {
			vehicles[i] = new Vehicle();
			vehicles[i].setOwner(OWNER);
			vehicles[i].changeSpeed(Math.random() * i);
			vehicles[i].setDirection(Math.random() * i);
		}
		
		LinkedListImpl<Vehicle> startNode = new LinkedListImpl<Vehicle>(vehicles[0]);
		
		LinkedListImpl<Vehicle> node = startNode;
		for(int i = 1; i < vehicles.length; i++) {
			node.setNext(vehicles[i]);
			node = node.next;
		}
		
		LinkedListImpl<Vehicle> cloned = startNode.clone();
		((Vehicle)cloned.next.value).setOwner("CHANGE");
		cloned.next.next.next = new LinkedListImpl<Vehicle>(new Vehicle("CHANGE"));
		
		System.out.println(startNode.toString());
		System.out.println(cloned.toString());
	}


}
