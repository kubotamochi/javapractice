package exercise4.exercise4_1;
/**
 * the class of the battery which extends EnergySource class
 * @author kubotamochi
 *
 */
public class Battery implements EnergySource {

	/** current rate of the charging in this object */
	private int chargingRate;
	
	@Override
	public boolean empty() {
		return chargingRate <= 0;
	}

	@Override
	public void fill() {
		setChargingRate(100);
	}

	public int getChargingRate() {
		return chargingRate;
	}

	public void setChargingRate(int chargingRate) {
		this.chargingRate = chargingRate;
	}

	@Override
	public void showRest() {
		System.out.println("残り" + this.chargingRate + "%");
		
	}
	
	

}
