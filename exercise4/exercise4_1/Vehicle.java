package exercise4.exercise4_1;

/**
 * A class which represents vehicle.
 * @author kubotamochi
 *
 */
public class Vehicle {
	/** the vehicle's IDNumber */
	private final int id;
	/** speed the vehicle goes at */
	private double speed;
	/** a direction　angle the vehicle goes in */
	private double direction;
	/** the vehicle owner's name */
	private String owner;
	/** the energy of this object */
	private EnergySource energy;
	/** the IDNumber which is given to next vehicle object */
	private static int nextID;
	/** The max angle. The direction is never over this value. */
	private static final double MAX_ANGLE = 360.00;
	/** The angle which is added to direction when this vehicle turns left. */
	private static final int TURN_LEFT = -90;
	/** The angle which is added to direction when this vehicle turns right. */
	private static final int TURN_RIGHT = 90;
	
	
	/** create a Vehicle object with setting id */
	public Vehicle() {
		id = nextID++;
	}
	
	/** create a Vehicle object with setting id and ownersName */
	public Vehicle(String firstOwner) {
		this();
		owner = firstOwner;
	}
	
	public void start() {
		if(energy.empty()) {
			energy.fill();
		}
	}
	
	@Override
	public String toString() {
		final String SPLIT = ", ";
		StringBuilder str = new StringBuilder();
		str.append(id);
		str.append("(");
		if(owner == null) {
			str.append("NotBeOwned");
		} else {
			str.append(owner);
		}
		str.append(SPLIT);
		str.append(speed);
		str.append(SPLIT);
		str.append(direction);
		str.append(")");
		
		return str.toString();
	}
	
	/**
	 * Changes this speed to param value.
	 * @param speed
	 */
	public void changeSpeed(double speed) {
		this.speed = speed;
	}
	
	/**
	 * Sets this speed to 0.
	 */
	public void stop() {
		this.speed = 0;
	}
	
	/** Makes this vehicle turn by degree of the param angle */
	public void turn(double angle) {
		direction = (direction + angle) % MAX_ANGLE;
	}
	
	
	public void turn(int angle ) {
		if(angle != TURN_LEFT && angle != TURN_RIGHT) {
			throw new IllegalArgumentException();
		}
		direction = direction + angle;
	}
	
	public int getId() {
		return id;
	}
	public double getSpeed() {
		return speed;
	}
	public double getDirection() {
		return direction;
	}
	public void setDirection(double direction) {
		this.direction = direction % MAX_ANGLE;
	}
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	public static int getNextID() {
		return nextID;
	}
	
	/**
	 * return the max ID number in all numbers which Vehicle class has given.
	 * @return the Max ID number
	 */
	public static int getMaxID() {
		return nextID - 1;
	}

	public EnergySource getEnergy() {
		return energy;
	}

	public void setEnergy(EnergySource energy) {
		this.energy = energy;
	}
	
	public static void main(String[] args) {
		Vehicle vehicle = new Vehicle(args[0]);
		vehicle.changeSpeed(Math.random()*100.00);
		vehicle.setDirection(Math.random()*100.00);
		vehicle.setEnergy(new Battery());
		
		vehicle.getEnergy().showRest();
		vehicle.start();
		vehicle.getEnergy().showRest();
		
		System.out.println(vehicle.toString());
	}


}
