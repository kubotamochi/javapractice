package exercise4.exercise4_1;

/**
 * The abstract class which represents the energy source
 * @author kubotamochi
 *
 */
interface EnergySource {
	
	/**
	 * judges if this energy is empty or not.
	 * @return if this energy is empty, returns true.
	 *         otherwise, return false.
	 */
	boolean empty();
	
	/**
	 * makes this energy full.
	 */
	void fill();
	
	/**
	 * outputs the rest of this energy.
	 */
	void showRest();

}
