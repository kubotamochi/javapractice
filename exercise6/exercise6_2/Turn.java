package exercise6.exercise6_2;

/**
 * The enum which has directions
 * @author kubotamochi
 *
 */
public enum Turn {
	TURN_LEFT, TURN_RIGHT
}
