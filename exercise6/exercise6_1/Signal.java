package exercise6.exercise6_1;

/**
 * The enum which has colors of the traffic signal
 * @author kubotamochi
 *
 */
public enum Signal {
	RED, YELLOW, GREEN
}
