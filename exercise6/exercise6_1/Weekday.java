package exercise6.exercise6_1;

/**
 * The enum which has days of the week
 * @author kubotamochi
 *
 */
public enum Weekday {
	SUNDAY, MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY
}
