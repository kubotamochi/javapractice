package exercise6.exercise6_3;

/**
 * The enum which has messegelevel.
 * @author kubotamochi
 *
 */
public enum MessageLevel {

	SILENT, TERSE, NORMAL, VERBOSE
}
