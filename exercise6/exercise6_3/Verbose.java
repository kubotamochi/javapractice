package exercise6.exercise6_3;

/**
 * The interface which manages verborsity
 * @author kubotamochi
 *
 */
public interface Verbose {

	void setVerbosity(MessageLevel message);
	MessageLevel getVerbosity();
}
