package exercise3.exercise3_7;

public class Attr {
	private final String name;
	private Object value = null;
	
	public Attr(String name) {
		this.name = name;
	}
	
	public Attr(String name, Object value) {
		this.name = name;
		this.value = value;
	}
	
	public Object setValue(Object newValue) {
		Object oldValue = value;
		value = newValue;
		return oldValue;
	}
	
	public String toString() {
		return name + "= '" + value + "'";
	}

	public String getName() {
		return name;
	}

	public Object getValue() {
		return value;
	}

}
