package exercise3.exercise3_7;

/**
 * The class which represents RGB color.
 * @author kubotamochi
 *
 */
public class ScreenColor {
	private int red;
	private int green;
	private int blue;
	public static final int MAX_VAL = 256;
	
	public ScreenColor(Object value) {
		//do nothing
	}
	
	private int createCorrectRangeVal(int val) {
		if(val > MAX_VAL) val = MAX_VAL;
		if(val < 0) val = 0;
		return val;
	}
	
	public int getRed() {
		return red;
	}
	public void setRed(int red) {
		this.red = createCorrectRangeVal(red);
	}
	public int getGreen() {
		return green;
	}
	public void setGreen(int green) {
		this.green = createCorrectRangeVal(green);
	}
	public int getBlue() {
		return blue;
	}
	public void setBlue(int blue) {
		this.blue = createCorrectRangeVal(blue);
	}
	
	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof ScreenColor)) {
			return false;
		}
		ScreenColor target = (ScreenColor)obj;
		if(getRed() != target.getRed() ||
		   getGreen() != target.getGreen() ||
		   getBlue() != target.getBlue()) {
			return false;
		}
		return true;
	}
	
	@Override
	public int hashCode() {
		return getRed() * 1000000 + getGreen() * 1000 + getBlue();
	}
	
	public static void main(String[] args) {
		ScreenColor sc = new ScreenColor(new Object());
		sc.setRed(256);
		sc.setGreen(256);
		sc.setBlue(256);
		System.out.println(sc.hashCode());
	}

}
