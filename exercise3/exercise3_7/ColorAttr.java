package exercise3.exercise3_7;

public class ColorAttr extends Attr {
	private ScreenColor myColor;
	
	public ColorAttr(String name, Object value) {
		super(name, value);
		decodeColor();
	}
	
	public ColorAttr(String name) {
		this(name, "transparent");
	}
	
	public ColorAttr(String name, ScreenColor value) {
		super(name, value.toString());
		myColor = value;
	}
	
	public Object setValue(Object newValue) {
		Object retval = super.setValue(newValue);
		decodeColor();
		return retval;
	}
	
	public ScreenColor setValue(ScreenColor newValue) {
		super.setValue(newValue.toString());
		ScreenColor oldValue = myColor;
		myColor = newValue;
		return oldValue;
	}
	
	public ScreenColor getColor() {
		return myColor;
	}
	
	protected void decodeColor() {
		if(getValue() == null) {
			myColor = null;
			return;
		}
		
		myColor = new ScreenColor(getValue());
	}
	
	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof ColorAttr)) {
			return false;
		}
		
		ColorAttr target = (ColorAttr)obj;
		if(!getName().equals(target.getName())) {
			return false;
		}
		
		if(!getValue().toString().equals(target.getValue().toString())) {
			return false;
		}
		
		if(!getMyColor().equals(target.getMyColor())) {
			return false;
		}
		
		return true;
		
	}
	
	@Override
	public int hashCode() {
		return myColor.hashCode();
	}

	public ScreenColor getMyColor() {
		return myColor;
	}

	public void setMyColor(ScreenColor myColor) {
		this.myColor = myColor;
	}

}
