package exercise3.exercise3_5;

/**
 * The class which counts the time to append string
 * @author kubotamochi
 *
 */
public class StringAppendBenchmark extends Benchmark {

	private String test = "A";
	private StringBuilder builder = new StringBuilder();
	
	@Override
	void benchmark() {
		builder.append(test);
	}
	
	public static void main(String args[]) {
		int count = Integer.parseInt(args[0]);
		long uniteTime = new StringConcatenateBenchmark().repeat(count);
		StringAppendBenchmark test = new StringAppendBenchmark();
		long appendTime = test.repeat(count);
		System.out.println(uniteTime + " vs " + appendTime);
	}

}
