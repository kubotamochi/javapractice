package exercise3.exercise3_5;

/**
 * The class which counts the time to concatenate strings.
 * @author kubotamochi
 *
 */
public class StringConcatenateBenchmark extends Benchmark {

	private String test = "";
	
	@Override
	void benchmark() {
		test += "A";
	}
	
	public String getTest() {
		return test;
	}
	
	public static void main(String[] args) {
		int count = Integer.parseInt(args[0]);
		StringConcatenateBenchmark test = new StringConcatenateBenchmark();
		long time = test.repeat(count);
		System.out.println(time);
		System.out.println(test.getTest());
	}
	

}
