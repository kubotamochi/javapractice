package exercise3.exercise3_12;

/**
 * The class which can sort any object
 * @author kubotamochi
 *
 */
public class SortHarness {
	private Object[] data;
	
	public SortHarness(Object[] data) {
		this.data = data;
	}
	
	public void sort() {
		mergeSort(data);
	}
	
	/**
	 * sorts the array of objects which is set at constructer by mergesort.
	 */
	public void mergeSort(Object[] data) {
		if(data.length > 1) {
			int m = data.length / 2;
			int n = data.length - m;
			Object[] a1 = new Object[m];
			Object[] a2 = new Object[n];
			for(int i = 0; i < m; i++) {
				a1[i] = data[i];
			}
			for(int i = 0; i < n; i++) {
				a2[i] = data[m+i];
			}
			mergeSort(a1);
			mergeSort(a2);
			merge(a1,a2,data);
	    }
	  }

	/**
	 * merges two arraies of objects
	 * @param elements1
	 * @param elements2
	 * @param merged
	 */
	private void merge(Object[] elements1, Object[] elements2, Object[] merged) {
		int i = 0;
		int j = 0;
	    while(i < elements1.length || j < elements2.length) {
	    	if(j >= elements2.length || 
	    			(i < elements1.length && compare(elements1[i], elements2[j]) < 0)) {
	    		merged[i+j] = elements1[i];
	    		i++;
	    	} else {
	    		merged[i+j] = elements2[j];
	    		j++;
	    	}
	    }
	}

	/**
	 * compares hashcodes of two objects
	 * @param obj1
	 * @param obj2
	 * @return if the first param is bigger, return 1.
	 * if the first param is smaller, return -1.
	 * if two param is equal, return 0.
	 */
	private int compare(Object obj1, Object obj2) {
		if(obj1.hashCode() == obj2.hashCode()) {
			return 0;
		}
		if(obj1.hashCode() < obj2.hashCode()) {
			return -1;
		}
		return 1;
	}
	
	public Object[] getData() {
		return data;
	}
	
	public static void main(String[] args) {
		Integer[] data = {1,100,11,9,20,10000,3000, 1111111111};
		SortHarness test = new SortHarness(data);
		test.sort();
		for(int i = 0; i < test.getData().length; i++) {
			System.out.println((Integer)(test.getData()[i]));
		}
	}

	
	
}
