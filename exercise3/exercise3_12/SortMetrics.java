package exercise3.exercise3_12;

/**
 * the class which keeps the result of counting use of method for sorting. 
 * @author kubotamochi
 *
 */
final class SortMetrics implements Cloneable {
	public long probeCnt;
	public long compareCnt;
	public long swapCnt;
	
	public void init() {
		probeCnt = compareCnt = swapCnt = 0;
	}
	
	public String toString() {
		StringBuilder str = new StringBuilder();
		str.append(probeCnt).append(" probes ")
		.append(compareCnt).append(" compares ")
		.append(swapCnt).append(" swaps ");
		
		return str.toString();
	}
	
	public SortMetrics clone() {
		try {
			return (SortMetrics)super.clone();
		} catch(CloneNotSupportedException e) {
			throw new InternalError(e.toString());
		}
	}
}
