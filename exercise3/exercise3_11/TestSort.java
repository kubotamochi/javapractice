package exercise3.exercise3_11;

public class TestSort {
	static double[] data = {0.3, 1.3e-2, 7.9, 3.17 };
	
	public static void main(String[] args) {
		SortDouble bsort = new SimpleSortDouble();
		SortMetrics metrics = bsort.sort(data);
		System.out.println("Metrics: " + metrics);
		for(int i = 0; i < data.length; i++) {
			System.out.println("\t" + data[i]);
		}
	}

}
