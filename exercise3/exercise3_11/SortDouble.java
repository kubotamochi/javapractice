package exercise3.exercise3_11;

/**
 * The class for sorting double-data
 * @author kubotamochi
 *
 */
abstract class SortDouble {

	private double[] values;
	private final SortMetrics curMetrics = new SortMetrics();
	
	/**
	 * Sorts param data and return a result of counting use of method. 
	 * @param data
	 * @return SortMetrics class in which 
	 * the number of probe, compare and swap while sorting are.
	 */
	public final SortMetrics sort(double[] data) {
		values = data;
		curMetrics.init();
		doSort();
		return getMetrics();
	}
	
	public final SortMetrics getMetrics() {
		return curMetrics.clone();
	}
	
	protected final int getDataLength() {
		return values.length;
	}
	
	/**
	 * get the value of data at param index
	 * @param i
	 * @return value of data
	 */
	protected final double probe(int i) {
		curMetrics.probeCnt++;
		//malti-time reference makes it possible 
		//to compare values without the compare method
		if(curMetrics.probeCnt > 1) {
			curMetrics.compareCnt++;
		}
		return values[i];
	}
	
	/**
	 * 
	 * @param i
	 * @param j
	 * @return if the former param is biggar than the latter, return 1.
	 *         if the former param is smaller than the latter, return -1
	 *         if both are equal, return 0;
	 */
	protected final int compare(int i, int j) {
		curMetrics.compareCnt++;
		double d1 = values[i];
		double d2 = values[j];
		if(d1 == d2) {
			return 0;
		}
		
		return (d1 < d2 ? -1 : 1);
	}
	
	/**
	 * swap two values.
	 * @param i
	 * @param j
	 */
	protected final void swap(int i, int j) {
		curMetrics.swapCnt++;
		double tmp = values[i];
		values[i] = values[j];
		values[j] = tmp;
	}
	
	/**
	 * sort method which this subclasses have to implement.
	 */
	protected abstract void doSort();
}
