package exercise3.exercise3_9;

/**
 * The Garage in which sevaral Vehicles can be stored. 
 * @author kubotamochi
 *
 */
public class Garage implements Cloneable {

	private Vehicle[] vehicles;
	
	public Garage(int storeNum) {
		vehicles = new Vehicle[storeNum];
	}
	
	/**
	 * If the param is be able to be stored, return true.
	 * Otherwise, return false.
	 * @param vehicle
	 * @return 
	 */
	public boolean setVehicle(Vehicle vehicle) {
		for(int i = 0; i < vehicles.length; i++) {
			if(vehicles[i] == null) {
				vehicles[i] = vehicle;
				return true;
			}
		}
		System.out.println("満車です");
		return false;
	}
	
	public Vehicle[] getVehicles() {
		return vehicles;
	}
	
	
	
	@Override
	public Garage clone() {
		Garage clone = null;
		try {
			clone = (Garage)super.clone();
			clone.vehicles = new Vehicle[vehicles.length];
			for(int i = 0; i < getStoredNum(); i++) {
				clone.setVehicle(this.vehicles[i].clone());
			}
		} catch(CloneNotSupportedException e) {
			System.out.println("複製できない乗り物が含まれています" + e.getMessage());
		}
		return clone;
	}
	
	/**
	 * get the number of vehicles which is stored at this garage.
	 * @return
	 */
	public int getStoredNum() {
		int cnt = 0;
		for(int i = 0; i < vehicles.length; i++) {
			if(vehicles[i] != null) {
				cnt++;
			}
		}
		return cnt;
	}
	
	@Override
	public String toString() {
		StringBuilder str = new StringBuilder();
		str.append(super.toString()).append("\n")
		.append("最大格納台数:").append(vehicles.length).append("台").append("\n")
		.append("現在格納台数:").append(getStoredNum()).append("台");
		return str.toString();
	}
	
	public static void main(String[] args) {
		Garage original = new Garage(5);
		Vehicle v1 = new PassengerVehicle("Owner", 4);
		v1.setEnergy(new GasTank(50));
		original.setVehicle(v1);
		
		System.out.println(original.toString());
		System.out.println(original.getVehicles()[0].getEnergy().empty());
		
		Garage clone = original.clone();
		clone.getVehicles()[0].getEnergy().fill();
		
		System.out.println(original.toString());
		System.out.println(original.getVehicles()[0].getEnergy().empty());
		System.out.println(clone.toString());
		System.out.println(clone.getVehicles()[0].getEnergy().empty());
	}
}
