package exercise3.exercise3_9;

/**
 * the class which supplies electricity to battery classes.
 * @author kubotamochi
 *
 */
public class Plug {
	
	/**
	 * supplies electricity to the param battery until it is full.
	 * @param battery
	 */
	public void fullCharge(Battery battery) {
		battery.setChargingRate(100);
	}

}
