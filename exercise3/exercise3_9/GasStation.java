package exercise3.exercise3_9;

/**
 * the class which serves GasTank class
 * @author kubotamochi
 *
 */
public class GasStation {

	/**
	 * fills up the param tank with the gas.
	 * @param tank
	 */
	public void fill(GasTank tank) {
		tank.setQuantity(tank.getMax());
	}
}
