package exercise3.exercise3_1;

import java.util.ArrayList;
import java.util.List;

/**
 * The class which extends Vehicle class.
 * This class can have seats and passengers.
 * @author kubotamochi
 *
 */
public class PassengerVehicle extends Vehicle {
	/** The number of seats in case no specific number is given */
	private static final int DEFAULT_SEAT_NUM = 4;
	
	/** the number of seats */
	private int seatNum;
	
	/** The list of passengers' names */
	private List<String> passengers = new ArrayList<String>();
	
	public PassengerVehicle() {
		this(DEFAULT_SEAT_NUM);
	}
	
	public PassengerVehicle(int seatNum) {
		super();
		this.seatNum = seatNum;
	}
	
	public PassengerVehicle(String firstOwner, int seatNum) {
		super(firstOwner);
		this.seatNum = seatNum;
	}
	
	@Override
	public String toString() {
		StringBuilder str = new StringBuilder(super.toString());
		str.append("\n").append("座席数：").append(getSeatNum()).append("席")
		.append("\n").append("乗客数：").append(getPassengerNum()).append("人");
		
		return str.toString();
	}
	
	/**
	 * Add param name to the passengers' names list 
	 * if this object has a space.
	 * @param name
	 */
	public void addPassenger(String name) {
		if(getPassengerNum() == seatNum) {
			System.out.println(name + "さんは満席で乗れません");
			return;
		}
		
		passengers.add(name);
		
	}
	
	/**
	 * Return the number of current passengers.
	 * @return
	 */
	public int getPassengerNum() {
		return passengers.size();
	}

	public int getSeatNum() {
		return seatNum;
	}

	public static void main(String[] args) {
		PassengerVehicle car1 = new PassengerVehicle();
		PassengerVehicle car2 = new PassengerVehicle(2);
		PassengerVehicle car3 = new PassengerVehicle("HOGE", 9);
		
		car1.addPassenger("takahashi");
		car1.addPassenger("noguchi");
		
		car2.addPassenger("higo");
		car2.addPassenger("ueshima");
		car2.addPassenger("jimon");
		
		System.out.println(car1.toString());
		System.out.println(car2.toString());
		System.out.println(car3.toString());
	}

}
