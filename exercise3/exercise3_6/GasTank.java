package exercise3.exercise3_6;

/**
 * the gas tank class which extends EnergySource class.
 * @author kubotamochi
 *
 */
public class GasTank extends EnergySource {
	
	/** the rest quantity of the gas */
	private double quantity;

	/** the max limit of the gas in this tank */
	private double max;
	
	public GasTank(double max) {
		this(max, 0);
	}
	
	public GasTank(double max, double defaultQuantity) {
		super();
		this.max = max;
		this.quantity = defaultQuantity;
	}
	
	@Override
	public boolean empty() {
		if(this.quantity <= 0) {
			return true;
		}
		return false;
	}

	@Override
	public void fill() {
		new GasStation().fill(this);
	}

	public double getQuantity() {
		return quantity;
	}

	public double getMax() {
		return max;
	}
	
	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}

	@Override
	public void showRest() {
		System.out.println("残り" + this.quantity + "リットル");
		
	}
	

}
