package exercise3.exercise3_6;

/**
 * The abstract class which represents the energy source
 * @author kubotamochi
 *
 */
abstract class EnergySource {
	
	/**
	 * judges if this energy is empty or not.
	 * @return if this energy is empty, returns true.
	 *         otherwise, return false.
	 */
	public abstract boolean empty();
	
	/**
	 * makes this energy full.
	 */
	public abstract void fill();
	
	/**
	 * outputs the rest of this energy.
	 */
	public abstract void showRest();

}
