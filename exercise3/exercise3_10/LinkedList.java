package exercise3.exercise3_10;

import exercise3.exercise3_10.vehicles.Vehicle;



/**
 * LinkedList class which has a value and reference to the next in the list.
 * @author kubotamochi
 *
 */
public class LinkedList implements Cloneable {

	/** the value of this object */
	private Object value;
	/** reference to the next object in the list */
	private LinkedList next;
	
	/** The constructor with object's value parameter */
	public LinkedList(Object value) {
		this.value = value;
	}
	
	/** The constructor with object's value and the next object parameters */
	public LinkedList(Object value, LinkedList next) {
		this(value);
		this.next = next;
	}
	
	@Override
	public LinkedList clone() throws CloneNotSupportedException {
		LinkedList cloned = (LinkedList)super.clone();
		if(this.next == null) {
			return cloned;
		}
		
		LinkedList originalNext = this.next;
		LinkedList target = cloned;
		
		while(originalNext != null) {
			target.next = new LinkedList(originalNext.value);
			originalNext = originalNext.next;
			target = target.next;
		}
		
		return cloned;
		
	}
	@Override
	public String toString() {
		StringBuilder str = new StringBuilder();
		str.append(value.toString());
		str.append(" ");
		if(next != null) {
			str.append("before of ");
			str.append(next.toString());
		}
		
		return str.toString();
	}

	/**
	 * Counts nodes of the List. 
	 * @return number of nodes.
	 */
	public int countNode() {
		int cnt = 1;
		LinkedList node = next;
		while(node != null) {
			cnt++;
			node = node.next;
		}
		
		return cnt;
	}
	
	public Object getValue() {
		return value;
	}
	public LinkedList getNext() {
		return next;
	}
	public void setNext(LinkedList next) {
		this.next = next;
	}
	
	public static void main(String[] args) throws CloneNotSupportedException {
		final int VEHICLE_NUM = 5;
		final String OWNER = "Kubota";
		Vehicle[] vehicles = new Vehicle[VEHICLE_NUM];
		
		 
		for(int i = 0; i < vehicles.length; i++) {
			vehicles[i] = new Vehicle();
			vehicles[i].setOwner(OWNER);
			vehicles[i].changeSpeed(Math.random() * i);
			vehicles[i].setDirection(Math.random() * i);
		}
		
		LinkedList startNode = new LinkedList(vehicles[0]);
		
		LinkedList node = startNode;
		for(int i = 1; i < vehicles.length; i++) {
			node.setNext(new LinkedList(vehicles[i]));
			node = node.next;
		}
		
		LinkedList cloned = startNode.clone();
		((Vehicle)cloned.next.value).setOwner("CHANGE");
		cloned.next.next.next = new LinkedList(new Vehicle("CHANGE"));
		
		System.out.println(startNode.toString());
		System.out.println(cloned.toString());
	}
}
