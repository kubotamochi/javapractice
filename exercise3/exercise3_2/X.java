package exercise3.exercise3_2;

public class X {
	protected int xMask = 0x00ff;
	protected int fullMask;
	
	{ 
		System.out.printf("Xのフィールド初期化 : %4x,     , %4x%n", xMask, fullMask);  
	}
	
	public X() {
		fullMask = xMask;
		System.out.printf("Xのコンストラクタ実行: %4x,     , %4x%n", xMask, fullMask);
	}
		
	public int mask(int orig) {
		return (orig & fullMask);
	}

}
