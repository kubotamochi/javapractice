package exercise3.exercise3_2;

public class Y extends X {

	protected int yMask = 0xff00;
	protected int defaultMask = 15;
	
	{ 
		System.out.printf("Yのフィールド初期化 : %4x, %4x, %4x%n", xMask, yMask, fullMask);  
	}
	
	public Y() {
		fullMask |= yMask;
		System.out.printf("Yのコンストラクタ実行: %4x, %4x, %4x%n", xMask, yMask, fullMask);
	}
	

	
	public static void main(String[] args) {
		Y y = new Y();
	}
	

}
